.. include:: ../README.rst

.. _table-of-contents:

Table of Contents
=================

.. toctree::
   :maxdepth: 2

   topics/support/index

.. toctree::
   :maxdepth: 2

   topics/install/index

.. toctree::
   :maxdepth: 2

   topics/contrib/contributing
   GitLab Repository <https://gitlab.com/saltstack/open/docs/salt-install-guide>
