.. |release| replace:: 3003.2
.. |delta-proxy-version| replace:: 3003.2
.. |solaris-version| replace:: 3003.1
.. |aix-version| replace:: 3003.1
.. |juniper-version| replace:: 3003.1
.. |juniper-file-version| replace:: 20210729-172533
.. |arista-version| replace:: 3003.1

.. |aix-python-version| replace:: Python 3.7.10
.. |arista-python-version| replace:: Python 3.7.10
.. |solaris-python-version| replace:: Python 3.7.10

.. |master| replace:: master
.. |masters| replace:: masters
.. |Master-cap| replace:: Master
.. |Masters-cap| replace:: Masters
.. |master-salt| replace:: Salt master
.. |masters-salt| replace:: Salt masters
.. |master-service| replace:: salt-master service
.. |minion| replace:: minion
.. |minions| replace:: minions
.. |Minion-cap| replace:: Minion
.. |Minions-cap| replace:: Minions
.. |minion-salt| replace:: Salt minion
.. |minions-salt| replace:: Salt minions
.. |minion-service| replace:: salt-minion service
.. |juniper| replace:: Juniper
.. |juniper-native-minion| replace:: Juniper native minion
.. |arista| replace:: Arista
.. |arista-native-minion| replace:: Arista native minion
.. |delta-proxy-minion| replace:: delta proxy minion
.. |aix| replace:: AIX
.. |aix-native-minion| replace:: AIX native minion
.. |solaris| replace:: Solaris
.. |solaris-native-minion| replace:: Solaris native minion
.. |delta-proxy-minions| replace:: delta proxy minions
