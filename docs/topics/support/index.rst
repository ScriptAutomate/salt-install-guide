.. _support:

=====================
Salt Platform Support
=====================

Before implementing Salt to manage any data center infrastructure it is
important to understand a few things about Salt platform support including:

* What platforms will the Salt Master run on?
* What systems and infrastructure can be managed by a Salt Minion?

Salt runs on and manages many versions of Linux, Windows, Mac OS X and UNIX.
The Salt Supported Operating Systems details the specific operating
systems that are fully supported and outlines the package creation policy for
each operating system listed. The page also outlines the best-effort
support policy for additional operating systems.

.. toctree::
   :maxdepth: 2
   :glob:

   *
