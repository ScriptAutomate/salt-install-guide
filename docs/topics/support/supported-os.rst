.. _support-os:

===========================
Supported Operating Systems
===========================

.. todo::

   This is a placeholder for:

   * `SaltStack Supported Operating Systems PDF <https://saltproject.io/wp-content/uploads/2021/02/SaltStack-Supported-Operating-Systems.pdf>`__
   * GitLab issue: `OS Support PDF moved to .rst file <https://gitlab.com/saltstack/open/docs/salt-install-guide/-/issues/1>`__
