.. _support-salt-lifecycle:

======================
Salt Version Lifecycle
======================

The :ref:`support-os` guidelines, in addition to these Salt product
lifecycle phases and definitions listed below, are intended to clearly define
how long a particular operating system and Salt version will receive official
packages, testing, and technical support.

.. todo::

   This is a placeholder for:

   * `Salt Version Tables and Policy Description <https://saltproject.io/salt-platform-support/>`__
   * GitLab issue: `Lifecycle Support page move to .rst file <https://gitlab.com/saltstack/open/docs/salt-install-guide/-/issues/2>`__
